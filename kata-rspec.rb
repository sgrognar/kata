# do not modify -----------------------------------------------------
class Map

  def initialize
    @map = Hash.new
  end

  def draw(x, y, str)
    @map[[x,y]] = str
  end

  def print
    pp @map
  end

  def [](location)
    @map[location]
  end
end
# -------------------------------------------------------------------

# modify below classes ----------------------------------------------
class Duck
end

class Cat
end

class Phone
end
# -------------------------------------------------------------------

# do not modify -----------------------------------------------------
describe 'phone and cat' do
  it 'needs to be able to draw its location on a map' do
    map   = Map.new

    phone = Phone.new
    phone.x_coord = 12
    phone.y_coord = 23
    phone.draw_location_on_map(map)
    expect(map[[phone.x_coord,phone.y_coord]]).to be == 'Phone'

    cat         = Cat.new
    cat.x_coord = 34
    cat.y_coord = 32
    cat.draw_location_on_map(map)
    expect(map[[cat.x_coord,cat.y_coord]]).to be == 'Cat'

  end
end

describe 'cat and duck' do
  it 'needs to be able to move' do
    cat         = Cat.new
    cat.x_coord = 34
    cat.y_coord = 32
    cat.move(10,-10)
    expect(cat.x_coord).to be == 44
    expect(cat.y_coord).to be == 22

    duck         = Duck.new
    duck.x_coord = 102
    duck.y_coord = 2
    duck.move(-2,-2)
    expect(duck.x_coord).to be == 100
    expect(duck.y_coord).to be == 0
  end
end

describe 'duck' do
  it 'must not be able to draw its location on a map' do
    duck = Duck.new
    expect(duck.respond_to?(:draw_location_on_map)).to be == false
  end 
end

#--------------------------------------------------------------------