# Exercise
- I Want to have a **Phone**, a **Cat**, and a **Duck**. 
- They all have a **x_coord** and **y_coord**.
- Cat and Duck must have a **move(x,y)** method
- I want to be able to **draw the location of (only) the phone and cat on a map**.

# How?
Modify the code below in the provided file.
```ruby
class Duck
end

class Cat
end

class Phone
end
```

Run the code to verify your solution.

# Running the code
- **If Ruby is installed on your machine**

You can choose if you want to run the code with/without rspec
    - With rspec: `rspec kata-rspec.rb`
    - Without rspec: `ruby kata-no_rspec.rb`
- **No Ruby installed**

You can run the code from `kata-no_rspec.rb` using an online Ruby interpreter (e.g.: https://repl.it/languages/ruby).