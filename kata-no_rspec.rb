# do not modify -----------------------------------------------------
class Map

  def initialize
    @map = Hash.new
  end

  def draw(x, y, str)
    @map[[x,y]] = str
  end

  def print
    pp @map
  end

  def [](location)
    @map[location]
  end
end
# -------------------------------------------------------------------

# modify below classes ----------------------------------------------
class Duck
end

class Cat
end

class Phone
end
# -------------------------------------------------------------------

# do not modify -----------------------------------------------------
def test_drawing_on_map
  map   = Map.new

  phone = Phone.new
  phone.x_coord = 12
  phone.y_coord = 23
  phone.draw_location_on_map(map)
  raise 'drawing on map failed for Phone' unless map[[phone.x_coord,phone.y_coord]] == 'Phone'

  cat         = Cat.new
  cat.x_coord = 34
  cat.y_coord = 32
  cat.draw_location_on_map(map)
  raise 'drawing on map failed for Cat'   unless map[[cat.x_coord,cat.y_coord]] == 'Cat'
end

def test_moving
  cat         = Cat.new
  cat.x_coord = 34
  cat.y_coord = 32
  cat.move(10,-10)
  raise 'moving of cat failed' unless cat.x_coord == 44 and cat.y_coord == 22

  duck         = Duck.new
  duck.x_coord = 102
  duck.y_coord = 2
  duck.move(-2,-2)
  raise 'moving of duck failed' unless duck.x_coord == 100 and duck.y_coord == 0
end

def test_not_drawing_on_map
  duck = Duck.new
  raise 'duck should not be able to draw its location' unless duck.respond_to?(:draw_location_on_map) == false
end

def test_implementation
  test_drawing_on_map
  test_moving
  test_not_drawing_on_map
end

test_implementation
#--------------------------------------------------------------------